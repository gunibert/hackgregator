use std::env;
use std::path::PathBuf;
use xshell::{cmd, Shell};

fn is_flatpak() -> bool {
    let container = env::var("container");
    if let Ok(container) = container {
        if container == "flatpak" {
            return true;
        }
    }
    false
}

fn install_binary(sh: &Shell) -> anyhow::Result<()> {
    let mut source = sh.current_dir();
    source.push("target/release/hackgregator");

    // assume for now that we always publish into a flatpak but track if we can see a flatpak
    // build if container as environment is set
    let is_flatpak = is_flatpak();
    let dest = if is_flatpak {
        let destdir = option_env!("FLATPAK_DEST");
        let destdir_str = destdir.unwrap_or("/app");
        let mut destdir_path = PathBuf::from(destdir_str);
        destdir_path.push("bin");
        destdir_path
            .into_os_string()
            .into_string()
            .unwrap_or(String::from("/app/bin"))
    } else {
        String::from("/usr/local/bin")
    };

    cmd!(sh, "install -Dm755 {source} -t {dest}").run()?;
    Ok(())
}

fn install_desktop_file(sh: &Shell) -> anyhow::Result<()> {
    sh.change_dir("hackgregator");
    // TODO: find the app-id programmatically
    let source = "data/de.gunibert.Hackgregator.desktop";

    // assume for now that we always publish into a flatpak but track if we can see a flatpak
    // build if container as environment is set
    let is_flatpak = is_flatpak();
    let dest = if is_flatpak {
        "/app/share/applications/"
    } else {
        "/usr/local/share/applications/"
    };
    cmd!(sh, "install -Dm644 {source} -t {dest}").run()?;
    Ok(())
}

fn install_icon(sh: &Shell) -> anyhow::Result<()> {
    // TODO: find the app-id programmatically
    let source = "./data/icons/hicolor/scalable/apps/de.gunibert.Hackgregator.svg";

    // assume for now that we always publish into a flatpak but track if we can see a flatpak
    // build if container as environment is set
    let is_flatpak = is_flatpak();
    let dest = if is_flatpak {
        "/app/share/icons/hicolor/scalable/apps/"
    } else {
        "/usr/local/share/icons/hicolor/scalable/apps/"
    };
    cmd!(sh, "install -Dm644 {source} -t {dest}").run()?;
    Ok(())
}

fn install_appstream(sh: &Shell) -> anyhow::Result<()> {
    // TODO: find the app-id programmatically
    let source = "./data/de.gunibert.Hackgregator.appdata.xml";

    // assume for now that we always publish into a flatpak but track if we can see a flatpak
    // build if container as environment is set
    let is_flatpak = is_flatpak();
    let dest = if is_flatpak {
        "/app/share/appdata/"
    } else {
        "/usr/local/share/appdata/"
    };
    cmd!(sh, "install -Dm644 {source} -t {dest}").run()?;
    Ok(())
}

fn main() -> anyhow::Result<()> {
    let sh = xshell::Shell::new()?;

    install_binary(&sh)?;
    install_desktop_file(&sh)?;
    install_icon(&sh)?;
    install_appstream(&sh)?;

    Ok(())
}
