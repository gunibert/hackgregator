-- Your SQL goes here

CREATE TABLE items (
    id INTEGER PRIMARY KEY NOT NULL,
    type TEXT,
    title TEXT,
    text TEXT,
    url TEXT,
    by TEXT,
    time DATETIME,
    score INTEGER
)