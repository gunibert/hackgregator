use crate::api::item::ItemData;
use std::collections::HashMap;

#[derive(Debug, Default)]
pub struct ItemStore {
    map: HashMap<i64, ItemData>,
}

impl ItemStore {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn add_item(&mut self, item: ItemData) {
        self.map.insert(item.id, item);
    }

    pub fn contains_item(&self, id: &i64) -> bool {
        self.map.contains_key(id)
    }

    pub fn get_item(&self, id: &i64) -> ItemData {
        self.map.get(id).unwrap().clone()
    }
}
