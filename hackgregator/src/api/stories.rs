use serde::Deserialize;

#[derive(Deserialize, Debug)]
pub Stories(Vec<i64>);
