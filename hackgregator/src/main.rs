mod api;
mod application;
mod commentspage;
mod commentsrow;
mod config;
mod db;
mod schema;
mod storyrow;
mod window;

use self::application::HackregatorRustApplication;
use self::window::HackregatorRustWindow;
use gettextrs::{bind_textdomain_codeset, bindtextdomain, textdomain};
use gtk::gio;
use gtk::prelude::*;
use log::debug;
use once_cell::sync::Lazy;

pub static RUNTIME: Lazy<tokio::runtime::Runtime> = Lazy::new(|| {
    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .unwrap()
});

fn main() {
    pretty_env_logger::init();

    debug!("Starting {}...", "de.gunibert.Hackgregator");

    // Set up gettext translations
    bindtextdomain(config::GETTEXT_PACKAGE, config::LOCALEDIR)
        .expect("Unable to bind the text domain");
    bind_textdomain_codeset(config::GETTEXT_PACKAGE, "UTF-8")
        .expect("Unable to set the text domain encoding");
    textdomain(config::GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    // Load resources
    gio::resources_register_include!("hackgregator.gresource").expect("Could not load resources");

    let app =
        HackregatorRustApplication::new(config::APPLICATION_ID, &gio::ApplicationFlags::empty());

    std::process::exit(app.run().into());
}
