// @generated automatically by Diesel CLI.

diesel::table! {
    items (id) {
        id -> Integer,
        #[sql_name = "type"]
        type_ -> Nullable<Text>,
        title -> Nullable<Text>,
        text -> Nullable<Text>,
        url -> Nullable<Text>,
        by -> Nullable<Text>,
        time -> Nullable<diesel::sql_types::TimestamptzSqlite>,
        score -> Nullable<Integer>,
        comments -> Nullable<Integer>,
    }
}
