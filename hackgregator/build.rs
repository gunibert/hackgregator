use std::io::Write;

fn create_config() {
    let prefix = option_env!("PREFIX").unwrap_or("/app/");

    let version = format!(
        "pub static VERSION: &str = \"{}\";",
        env!("CARGO_PKG_VERSION")
    );
    let gettext_package = format!(
        "pub static GETTEXT_PACKAGE: &str = \"{}\";",
        env!("CARGO_PKG_NAME")
    );

    let localedir = format!(
        "pub static LOCALEDIR: &str = \"{}{}\";",
        prefix, "share/locale"
    );
    let pkgdatadir = format!(
        "pub static PKGDATADIR: &str = \"{}{}{}\";",
        prefix,
        "share/",
        env!("CARGO_PKG_NAME")
    );

    let application_id = "pub static APPLICATION_ID: &str = \"de.gunibert.Hackgregator\";";
    let mut config_rs =
        std::fs::File::create("src/config.rs").expect("Could not create config file");
    write!(
        config_rs,
        "{version}\n{gettext_package}\n{localedir}\n{pkgdatadir}\n{application_id}\n"
    )
    .expect("Boom");
}

fn main() {
    create_config();
    glib_build_tools::compile_resources(
        &["src"],
        "src/hackgregator.gresource.xml",
        "hackgregator.gresource",
    );
}
